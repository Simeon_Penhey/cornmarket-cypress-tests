describe('MyCornmarket', function(){
	it('Check the title is correct', function(){
	cy.visit('https://www.mycornmarkettest.com')
	cy.title().should('include', 'MyCornmarket')
	})

	context('Login page', function(){
		beforeEach(function(){
			cy.visit('https://www.mycornmarkettest.com/')
		})

		it('Find the title and checks it is correct', function(){
			cy.contains('Login').should('contain','Login')
		})

		it('Click the Forgotten Password? button', function(){
			cy.contains('Forgotten Password?').click()
			cy.url().should('include','/reset-password')
		})

		it('Click the Register button', function(){
			cy.contains('Register').click()
			cy.url().should('include','/register')
		})

		it('Click the Privacy Statement button', function(){
			cy.contains('Privacy Statement').click()
			cy.url().should('include','/page/privacy')
		})

		it('Click the Terms & Conditions button', function(){
			cy.contains('Terms & Conditions').click()
			cy.url().should('include','/page/terms')
		})

		it('Check the Sign in button is disabled without data', function(){
			cy.contains('SIGN IN').should('be.disabled')
		})

		it('Check the invalid fields errors appears', function(){
			cy.contains('Email Address').click({ force: true })
			cy.contains('Password').click({ force: true })
			cy.wait(500)
			cy.contains('Email Address is required').should('be.visible')
			cy.contains('Email Address').click({ force: true })
			cy.wait(500)
			cy.contains('Password is required').should('be.visible')
		})

		it('Enter invalid credentials', function(){
			cy.get('md-input-container').find('#input_0').type('afakeemail@notreal.com',{ force: true })
			cy.get('md-input-container').find('#input_1').type('123456789',{ force: true })
			cy.contains('SIGN IN').click()
			cy.get('div.toast.toast-error').should('be.visible')
			
		})

		it('Logs in', function(){
			cy.get('md-input-container').find('#input_0').type('x-1129109i@dummyemail.com',{ force: true })
			cy.get('md-input-container').find('#input_1').type('pass1234',{ force: true })
			cy.contains('SIGN IN').click()
			cy.url().should('include','/home')
		})
	})

	context('Home page', function(){

		beforeEach(function(){
			cy.visit('https://www.mycornmarkettest.com/')
			cy.get('md-input-container').find('#input_0').type('x-1129109i@dummyemail.com',{ force: true })
			cy.get('md-input-container').find('#input_1').type('pass1234',{ force: true })
			cy.contains('SIGN IN').click()
		})

		it('Click the My policies button in the side bar',function(){
			cy.get('li.ng-scope').first().next().click()
			cy.url().should('not.include', '/home')
		})

		it('Click the Renew policy button in the side bar',function(){
			cy.get('li.ng-scope').first().next().next().click()
			cy.url().should('include', '/renew-policy')
		})

		it('Click the get a quote button in the side bar',function(){
			cy.get('li.ng-scope').first().next().next().next().click()
			cy.url().should('include', '/get-quote')
		})

		it('Click the Tools - Reactivation button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().click()
			.find('ul#menu-Tools.menu-toggle-list').find('li.ng-scope').first().click()
			cy.url().should('include', '/reactivation')
		})

		it('Click the Tools - Sick pay calc button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().click()
			.find('ul#menu-Tools.menu-toggle-list').find('li.ng-scope').first().next().click()
			cy.url().should('include', '/sick-pay-calculator')
		})

		it('Click the Tools - Tax refund calculator button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().click()
			.find('ul#menu-Tools.menu-toggle-list').find('li.ng-scope').last().click()
			cy.url().should('include', '/tax-calculator')
		})

		it('Click the My important dates button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().click()
			cy.url().should('include', '/my-important-dates')
		})

		it('Click the Contact us button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().click()
			cy.url().should('include', '/contactus')
		})

		it('Click the Our products and services - Car insurance button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9jYXItaW5zdXJhbmNlLWluZm8=')
		})

		it('Click the Our products and services - Home insurance button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9ob21lLWluc3VyYW5jZS1pbmZv')
		})

		it('Click the Our products and services - Health insurance button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9oZWFsdGgtaW5zdXJhbmNlLWluZm8=')
		})

		it('Click the Our products and services - Salary protection button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9pbmNvbWUtcHJvdGVjdGlvbi1pbmZv')
		})

		it('Click the Our products and services - AVCs button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9hdmNzLWluZm8=')
		})

		it('Click the Our products and services - Financial planning service button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9maW5hbmNpYWwtcGxhbm5pbmctc2VydmljZS1pbmZv')
		})

		it('Click the Our products and services - Retirement planning service button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9yZXRpcmVtZW50LXBsYW5uaW5nLXNlcnZpY2UtaW5mbw==')
		})

		it('Click the Our products and services - Life insurance button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9saWZlLWluc3VyYW5jZS1pbmZv')
		})

		it('Click the Our products and services - Foursight savings plans button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9mb3Vyc2lnaHQtc2F2aW5ncy1wbGFuLWluZm8=')
		})

		it('Click the Our products and services - Tax return service button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().next().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS90YXgtcmV0dXJuLXNlcnZpY2UtaW5mbw==')
		})

		it('Click the Our products and services - Dental insurance button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').first().next().next().next().next().next().next().next().next().next().next().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS9kZW50YWwtaW5zdXJhbmNlLWluZm8=')
		})

		it('Click the Our products and services - Travel insurance button in the side bar', function(){
			cy.get('li.ng-scope').first().next().next().next().next().next().next().next().click()
			.find('li.ng-scope').last().click()
			cy.url().should('include', '/frame/aHR0cHM6Ly93d3cuY29ybm1hcmtldC5pZS90cmF2ZWwtaW5zdXJhbmNlLWluZm8=')
		})

		it('Click the Terms & conditions button in the side bar', function(){
			cy.get('li.ng-scope').last().prev().click()
			cy.url().should('include', '/termsandconditions')
		})

		it('Click the Logout button in the side bar', function(){
			cy.get('li.ng-scope').last().click()
			cy.url().should('include', '/login')
		})

		it('Click the My Policies button in the main body',function(){
			cy.wait(1000)
			cy.get('md-card.cornmarket-clicked').first().click()
			cy.url().should('not.include', '/home')
		})

		it('Click the Get a Quote button in the main body', function(){
			cy.wait(1000)
			cy.get('div.home-page.layout-fill.ng-scope').find('div.ng-scope.flex').click()
			cy.url().should('include', '/get-quote')
		})

		it('Click the Contact Us button in the main body',function(){
			cy.wait(1000)
			cy.get('div.home-page.layout-fill.ng-scope').find('div.flex').last().click()
			cy.url().should('include', '/contactus')
		})
	})
})