describe('Kitchen Sink', function(){
  it('.should() - assert that <title> is correct', function(){
    // https://on.cypress.io/visit
    cy.visit('https://nimbletank.com')

    // Here we've made our first assertion using a '.should()' command.
    // An assertion is comprised of a chainer, subject, and optional value.

    // https://on.cypress.io/should
    // https://on.cypress.io/and

    // https://on.cypress.io/title
    cy.title().should('include', 'nimbletank')
    //   ↲               ↲            ↲
    // subject        chainer      value

  })
  context('Check Title', function(){

  beforeEach(function(){
      cy.visit('https://nimbletank.com')
    })

    it('Finds the title and checks it is correct', function(){
      cy.get('h2.homepage').should('contain','Experience Transformation')
    })

})
  context('Contact Us', function(){
     beforeEach(function(){
      cy.visit('https://nimbletank.com')
    })
     it('Check the Contact Us button works', function(){
      cy.contains('Contact us').click()
      //check new URL loaded
      cy.url().should('include', '/contact.html')
     })
  })

  context('View Reel', function(){
    beforeEach(function(){
      cy.visit('https://nimbletank.com')
    })
    it('Check the View reel button works', function(){
      cy.contains('View reel').wait(500).click().wait(2500)
      //check Vimeo overlay appears
      cy.get('div.modal-body').should('be.visible')
    })
  })
  
  //Check the carousel
  context('Client Carousel', function(){
    beforeEach(function(){
      cy.visit('https://nimbletank.com')
    })
    it('Check the default carousel options are correct', function(){
      cy.wait(500)
      cy.contains('Santander').contains('Transforming Finance').should('be.visible')
      cy.contains('AWAL').contains('Enterprise Apps').should('be.visible')
      cy.contains('Fortius').contains('Transforming Health').should('be.visible')
      cy.contains('OneTrace').contains('Innovating Golf').should('be.visible')
      cy.contains('Vox Cinemas').contains('Service Design').should('not.be.visible')
      cy.contains('Forbidden Technologies').contains('Touch first Enterprise product').should('not.be.visible')
    })
    it('Check the right carousel navigation button works', function(){
      cy.wait(500)
      cy.get('i.icon-ic_right-arrow-thick').click({force:true})
      cy.contains('Santander').contains('Transforming Finance').should('not.be.visible')
      cy.contains('AWAL').contains('Enterprise Apps').should('be.visible')
      cy.contains('Fortius').contains('Transforming Health').should('be.visible')
      cy.contains('OneTrace').contains('Innovating Golf').should('be.visible')
      cy.contains('Vox Cinemas').contains('Service Design').should('be.visible')
      cy.contains('Kobalt').contains('Enterprise Apps').should('not.be.visible')
    })
    it('Check the left carousel navigation button works', function(){
      cy.wait(500)
      cy.get('i.icon-ic_left-arrow-thick').click({force:true})
      cy.contains('Forbidden Technologies').contains('Touch first Enterprise product').should('be.visible')
      cy.contains('Santander').contains('Transforming Finance').should('be.visible')
      cy.contains('AWAL').contains('Enterprise Apps').should('be.visible')
      cy.contains('Fortius').contains('Transforming Health').should('be.visible')
      cy.contains('OneTrace').contains('Innovating Golf').should('not.be.visible')
      cy.contains('soundjack').contains('Mobile-first service').should('not.be.visible')
    })
    it('Check carousel responsiveness', function(){
      cy.viewport(999,999)
      cy.contains('Santander').contains('Transforming Finance').should('be.visible')
      cy.contains('AWAL').contains('Enterprise Apps').should('be.visible')
      cy.contains('Fortius').contains('Transforming Health').should('be.visible')
      cy.contains('OneTrace').contains('Innovating Golf').should('not.be.visible')
    })
  })

  //Check the Clients section
  context('Clients', function(){
    beforeEach(function(){
      cy.visit('https://nimbletank.com')
    })
    it('Check the clients', function(){
      cy.get('div.col-sm-2.col-xs-6').first().should('have.html', '<a href="/bbc.html"><i class="icon-ic_bbc i-lg"></i></a>')
      .next().should('have.html','<a href="/universal.html"><i class="icon-ic_universal-music-group i-lg"></i></a>')
      .next().should('have.html', '<a href="/asos.html"><i class="icon-ic_asos i-lg"></i></a>')
      .next().should('have.html', '<a href="/jameson-whiskey.html"><i class="icon-ic_jameson i-lg"></i></a>')
      .next().should('have.html', '<a href="javascript:void(0);"><i class="icon-ic_electrolux i-lg"></i></a>')
      .next().should('have.html', '<a href="/kitti.html"><i class="icon-ic_santander i-lg"></i></a>')
      cy.get('div.col-sm-2.col-xs-6').last().should('have.html','<a href="/tizen.html"><i class="icon-ic_tizen i-lg"></i></a>')
      .prev().should('have.html','<a href="/vox.html"><i class="icon-ic_crowdsurf i-lg"></i></a>')
      .prev().should('have.html','<a href="/minicabit.html"><i class="icon-ic_minicabit i-lg"></i></a>')
      .prev().should('have.html','<a href="/soundjack.html"><i class="icon-ic_soundjack i-lg"></i></a>')
      .prev().should('have.html','<a href="/thomson-reuters-enterprise.html"><i class="icon-ic_thomson-reuters i-lg"></i></a>')
      .prev().should('have.html','<a href="/ovo-energy.html"><i class="icon-ic_ovo i-lg"></i></a>')
      cy.get('div.col-sm-2.col-xs-6.margin-bottom').should('have.html','<a href="/skysports.html"><i class="icon-ic_sky i-lg"></i></a>')
      cy.get('div.row.spaces').first().next().find('div.col-sm-2.col-xs-6').first().should('have.html','<a href="/skysports.html"><i class="icon-ic_sky i-lg"></i></a>')
      .next().should('have.html','<a href="/aimia.html"><i class="icon-ic_aimia i-lg"></i></a>')
      .next().should('have.html','<a href="/kobalt.html"><i class="icon-kobalt_logo i-md"></i></a>')
      .next().should('have.html','<a href="/orange.html"><i class="icon-ic_orange i-lg"></i></a>')
  })
    //If I was doing this seriously, I'd add a test for clicking each client icon
})

  
  
})